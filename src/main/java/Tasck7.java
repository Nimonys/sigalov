import java.util.Scanner;

public class Tasck7 {
    public static void main(String[] args) {

        String simvol = null;
        char preob;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter a character");
        simvol = in.nextLine();

        preob = simvol.charAt(0);

        if (((preob >= 'a') && (preob <= 'z')) || ((preob >= 'A') && (preob <= 'Z')))
            System.out.println("Latin");
        else if (((preob >= 'а') && (preob <= 'я')) || ((preob >= 'А') && (preob <= 'Я')))
            System.out.println("Cyrillic");
        else if ((preob >= '0') && (preob <= '9'))
            System.out.println("Number entered");
        else
            System.out.println("The character does`t belong to the Latin or Cyrillic alphabet");

    }
}
