import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        int choice;

        Scanner in = new Scanner(System.in);
        System.out.println("Choose an animal from the list:\n1-Cat\n2-Dog\n3-Cow\n4-Donkey\n5-Monkey\n6-Coyote");
        System.out.println("7-Mouse\n8-Snake\n9-Badger\n10-Hedgehog");
        choice = in.nextInt();

        switch (choice) {
            case 1:
                System.out.println("Miau");
                break;
            case 2:
                System.out.println("Gav");
                break;
            case 3:
                System.out.println("Muuu");
                break;
            case 4:
                System.out.println("Ia");
                break;
            case 5:
                System.out.println("Uuui");
                break;
            case 6:
                System.out.println("Iauuu");
                break;
            case 7:
                System.out.println("Pisc");
                break;
            case 8:
                System.out.println("Sssss");
                break;
            case 9:
                System.out.println("Nggggg");
                break;
            case 10:
                System.out.println("Frc");
                break;
        }
    }
}
