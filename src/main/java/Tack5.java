import java.util.Scanner;

public class Tack5 {
    public static void main(String[] args) {
        int den;
        int mes;
        int god;
        String znakZodiaca = " ";
        String animals = " ";

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter day:");
        den = sc.nextInt();
        System.out.println("Enter month:");
        mes = sc.nextInt();
        System.out.println("Enter the year:");
        god = sc.nextInt();

        if (den > 0 && den < 31) {
            if (mes >= 1 && mes <= 12) {
                if (god <= 1900)
                    System.out.println("Ошибка записи года");
            }
            else System.out.println("Ошибка записи месяца");
        }
        else System.out.println("Ошибка записи дня");

        switch (mes) {
            case 1:
                if (den <= 19)
                    znakZodiaca = "Capricorn";
                else
                    znakZodiaca = "Aquarius";
                break;
            case 2:
                if (den <= 18)
                    znakZodiaca = "Aquarius";
                else
                    znakZodiaca = "Fish";
                break;
            case 3:
                if (den <= 20)
                    znakZodiaca = "Fish";
                else
                    znakZodiaca = "Aries";
                break;
            case 4:
                if (den <= 19)
                    znakZodiaca = "Aries";
                else
                    znakZodiaca = "Taurus";
                break;
            case 5:
                if (den <= 20)
                    znakZodiaca = "Taurus";
                else
                    znakZodiaca = "Twins";
                break;
            case 6:
                if (den <= 20)
                    znakZodiaca = "Twins";
                else
                    znakZodiaca = "Crayfish";
                break;
            case 7:
                if (den <= 22)
                    znakZodiaca = "Crayfish";
                else
                    znakZodiaca = "Lion";
                break;
            case 8:
                if (den <= 22)
                    znakZodiaca = "Lion";
                else
                    znakZodiaca = "Virgo";
                break;
            case 9:
                if (den <= 22)
                    znakZodiaca = "Virgo";
                else
                    znakZodiaca = "Libra";
                break;
            case 10:
                if (den <= 22)
                    znakZodiaca = "Libra";
                else
                    znakZodiaca = "Scorpio";
                break;
            case 11:
                if (den <= 21)
                    znakZodiaca = "Scorpio";
                else
                    znakZodiaca = "Sagittarius";
                break;
            case 12:
                if (den <= 21)
                    znakZodiaca = "Sagittarius";
                else
                    znakZodiaca = "Capricorn";
                break;
            default:
                break;
        }

        god = ((god - 1900) % 12) + 1;

        switch (god) {
            case 1:
                animals = "Rat";
                break;
            case 2:
                animals = "Bull";
                break;
            case 3:
                animals = "Tiger";
                break;
            case 4:
                animals = "Rabbit";
                break;
            case 5:
                animals = "Dragon";
                break;
            case 6:
                animals = "Snake";
                break;
            case 7:
                animals = "Horse";
                break;
            case 8:
                animals = "Goat";
                break;
            case 9:
                animals = "Monkey";
                break;
            case 10:
                animals = "Rooster";
                break;
            case 11:
                animals = "Dog";
                break;
            case 12:
                animals = "Pig";
                break;
            default:
                break;
        }

        System.out.println("You star sign: " + znakZodiaca);
        System.out.println("Your animal: " + animals);

    }
}
