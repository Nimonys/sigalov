import java.util.Scanner;

// Дана точка на плоскости заданная координатами x и y,
//определить и вывести в консоль, в какой четверти находится точка, в прямоугольной (декартовой) системе координат.
//Четверти обозначены римскими цифрами.

public class Task4 {
    public static void main(String[] args){

        int x;
        int y;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the x and y coordinates from -10 to +10:");
        x = in.nextInt();
        y = in.nextInt();

        if ((x > 0 && x <= 10) && (y > 0 && y <= 0))
            System.out.println("First quarter");
        else if ((x < 0 && x >= -10) && (y > 0 && y <= 0))
            System.out.println("Fourth quarter");
        else if ((x> 0 && x <= 10) && (y < 0 && y >= -10))
            System.out.println("Second quarter");
        else if ((x < 0 && x >= -10) && (y < 0 && y >= -10))
            System.out.println("Third quarter");
        else
            System.out.println("In the center of the x and y axis");
    }
}
