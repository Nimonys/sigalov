import java.util.Scanner;

// Определить номер подъезда девятиэтажного дома, по
//указанному номеру квартиры N. Считать, что на каждом
//этаже находится M квартир. Вывести в консоль номер подъезда.

public class Task10 {
    public static void main(String[] args) {

        int num;
        int kolvoKvar;
        int podezd;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter apartment number:");
        num = in.nextInt();
        System.out.println("Enter the number of apartments per floor:");
        kolvoKvar = in.nextInt();

        podezd = (num / kolvoKvar + 1) / 9 + 1;

        System.out.println("Entrance number: " + podezd);

    }
}
