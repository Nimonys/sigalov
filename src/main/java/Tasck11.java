import java.util.Scanner;

public class Tasck11 {
    public static void main(String[] args) {

        double a, b, c;
        double D;

        System.out.println("Given a quadratic equation of the form:");
        System.out.println("a*x^2 + b*x + c = 0");
        System.out.println("Input a, b, c:");

        Scanner in = new Scanner(System.in);
        a = in.nextDouble();
        b = in.nextDouble();
        c = in.nextDouble();

        D = b * b - 4 * a * c;

        if (D > 0) {
            double x1, x2;
            x1 = (-b - Math.sqrt(D)) / (2 * a);
            x2 = (-b + Math.sqrt(D)) / (2 * a);
            System.out.println("Roots of the equation: x1 = " + x1 + "x2 = " + x2);
        } else if (D == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println("Equation has a single root x = " + x);
        } else
            System.out.println("Equation has no roots");


    }
}
