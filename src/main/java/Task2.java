import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        int time;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the time:");
        time = in.nextInt();

        switch (time) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
                System.out.println("Goodnight");
                break;
            case 6:
            case 7:
            case 8:
            case 9:
                System.out.println("Good morning");
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                System.out.println("Good day");
                break;
            case 16:
            case 17:
            case 18:
            case 19:
                System.out.println("Good evening");
                break;
        }
    }
}
