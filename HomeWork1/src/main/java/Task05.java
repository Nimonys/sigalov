public class Task05 {
    public static void main(String[] args) {
        final int R = 5;
        double x = Math.PI * (R * R);
        double y = Math.PI * 2 * R;
        System.out.println("Радиус окружности = " + x);
        System.out.println("Длинна окружности = " + y);
    }
}
