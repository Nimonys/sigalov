public class Task06 {
    public static void main(String[] args) {
        final int w = 5;
        final int h = 10;
        int perimetr = 2 * w + 2 * h;
        int plochad = w * h;
        System.out.println("Периметр = " + perimetr);
        System.out.println("Площадь = " + plochad);
    }
}
