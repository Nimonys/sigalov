import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(n / 86400 + " Дней " + (n % 86400) / 3600 + " Часов " + ((n % 86400) % 3600) / 60 + " Минут " + n % 60 + " Секунд");
    }
}
