import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число учащихся:");
        int n = in.nextInt();
        double n1;
        double n2;
        double pir = 0;
        double mol = 0;
        int proc = 1;
        if (proc == 100) {
            pir = n * 2;
            mol = n * 0.2;
            mol = Math.ceil(mol / 0.9);
            System.out.println("Вам необходимо " + pir + " пирожков");
            System.out.println("Вам необходимо " + mol + " пакетов молока");
        } else if (proc == 60) {
            n1 = Math.ceil(n * 0.6);
            n2 = n - n1;
            pir = n2 + (n1 * 2);
            mol = n1 * 0.2;
            mol = Math.ceil(mol / 0.9);
            System.out.println("Вам необходимо " + pir + " пирожков");
            System.out.println("Вам необходимо " + mol + " пакетов молока");
        } else if (proc == 1) {
            n1 = Math.ceil(n * 0.1);
            n2 = n - n1;
            pir = n2 + (n1 * 2);
            mol = n1 * 0.2;
            mol = Math.ceil(mol / 0.9);
            System.out.println("Вам необходимо " + pir + " пирожков");
            System.out.println("Вам необходимо " + mol + " пакетов молока");
        }
    }
}
