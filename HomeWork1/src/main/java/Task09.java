public class Task09 {
    public static void main(String[] args) {
        double x = 2.0;
        double y;
        y = x - Math.round(x);
        if (y != 0) {
            System.out.println("Число имеет вещественную часть");
        } else {
            System.out.println("Число не имеет вещественной части");
        }
    }
}
