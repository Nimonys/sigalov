public class Task02 {
    public static void main(String[] args) {
        int n = 345;
        int y = 0;
        for (int i = 0; i < 3; i++) {
            y += n % 10;
            n /= 10;
        }
        System.out.println("Сумма = " + y);
    }
}
