import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input number1: ");
        int number1 = in.nextInt();
        System.out.println("Input number2: ");
        int number2 = in.nextInt();

        number1 = number1 | (1 << number2);

        System.out.println("Result: " + number1);
    }
}
