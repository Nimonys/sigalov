import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter number1: ");
        int number1 = in.nextInt();
        System.out.println("Enter number2: ");
        int number2 = in.nextInt();

        number1 = number1 > 0 ? 1 : 0;
        number2 = number2 > 0 ? 1 : 0;

        if (number1  > 0) {
            System.out.println("Positive number");
        }
        else
            System.out.println("The number is negative");

        if (number2 > 0) {
            System.out.println("Positive number");
        }
        else
            System.out.println("The number is negative");

    }
}
