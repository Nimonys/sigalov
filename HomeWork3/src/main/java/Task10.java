import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {

        int prom1 = 0;
        int prom2 = 0;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter number1: ");
        int num1 = in.nextInt();
        System.out.println("Enter number2: ");
        int num2 = in.nextInt();

        for (int i = 0; i <= num1 & i <= num2; i++) {
            prom1 = (num1 ^ i) == 0 ? 1 : 0;
            prom2 = (num2 ^ i) == 0 ? 1 : 0;
        }

        prom1 = ((prom1 & (1 << prom2)) == 0) ? 1 : 0;

        if (prom1 == 1) {
            System.out.println("Number 1 is greater then number 2");
        }
        else
            System.out.println("Number 2 is greater then number 1");

    }
}
