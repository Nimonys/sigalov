import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number: ");
        int number = in.nextInt();
        System.out.println("Enter position: ");
        int position = in.nextInt();

        number = number ^ (1 << (position - 1));

        System.out.println(Integer.toBinaryString(number));
    }
}
