import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {

        int counter = 0;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = in.nextInt();

        for (int a, i = 1; i <= number; ) {
            a = (number & i) > 0 ? 1 : 0;
            if (a == 1) {
                counter++;
            }
            i = i << 1;
        }

        System.out.println(Integer.toBinaryString(number));
        System.out.println("Counter: " + counter);
    }
}
