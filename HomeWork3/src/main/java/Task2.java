import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        int number = 2;

        Scanner in = new Scanner(System.in);
        System.out.println("Enter degree: ");
        int degree = in.nextInt();

        number = 2 << (degree >> 1);

        System.out.println("2 to the degree: " + number);
    }
}
